
export interface ICompaniesListItem {
  id: string;
  title: string;
  logoUrl: string;
  description: string;
};

export interface ICompaniesList {
  companies: ICompaniesListItem[]
};