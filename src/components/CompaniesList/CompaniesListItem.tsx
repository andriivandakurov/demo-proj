import React, { FC } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ICompaniesListItem } from "./types";
import CompanyLogo from "../Company/CompanyLogo";
import CompanyDescription from "../Company/CompanyDescription";

const CompanyHeader = styled.h3`
  color: green;
  text-transform: capitalize;
`;

const CompaniesListItem = styled.div`
  padding: 0 0 20px;
  animation: fade-in-keyframes 0.5s ease-out;
`;

const Component: FC<ICompaniesListItem> = ({
  title,
  logoUrl,
  description,
  id: companyId
}) => {
  return (
    <CompaniesListItem>
      <Link to={`/company/${companyId}`}>
        <CompanyHeader>{title}</CompanyHeader>
      </Link>

      <CompanyLogo
        style={{ width: "80px", height: "80px" }}
        src={`${logoUrl ? logoUrl + "?size=80" : ""}`}
      />
      <CompanyDescription>{description}</CompanyDescription>
    </CompaniesListItem>
  );
};

Component.displayName = "CompaniesListItem";

export default Component;
