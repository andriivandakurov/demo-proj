import React, { FC } from "react";
import CompaniesListItem from "./CompaniesListItem";
import { ICompaniesList } from "./types";
import styled from "styled-components";

const CompaniesList = styled.ul`
  max-width: 700px;
  margin: 0 auto;
  padding: 0;
`;

const Component: FC<ICompaniesList> = ({ companies }) => {
  return (
    <CompaniesList>
      {companies.map(company => (
        <CompaniesListItem key={company.id} {...company} />
      ))}
    </CompaniesList>
  );
};

Component.displayName = "CompaniesList";

export default Component;
