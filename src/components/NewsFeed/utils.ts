export const getLatestNews = (newsCount: number = 3): Promise<any> => {
  const url = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${process.env.REACT_APP_NEWS_API_KEY}`;
  const req = new Request(url);

  return fetch(req)
    .then(r => r.json())
    .then(r => r.articles)
    .then(a => a.slice(0, newsCount));
};
