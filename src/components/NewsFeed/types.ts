export interface INewsItem {
  title: string;
  description: string;
  summary: string;
  date: string;
  pubdate: string;
  pubDate: string;
  link: string;
  url?: string; 
  guid: string;
  author: string | null;
  comments: string | null;
  origlink: string | null;
}