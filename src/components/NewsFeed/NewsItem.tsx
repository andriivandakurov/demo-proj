import React, { FC } from "react";
import { INewsItem } from "./types";
import styled from "styled-components";

const NewsItem = styled.a`
  display: block;
  margin: 20px 0 40px;
`

const NewsItemHeader = styled.h3`
  margin: 0;
`
const NewsItemAuthor = styled.p`
  margin: 0;
`

const NewsItemDescription = styled.p`
  margin: 10px 0;
  font-style: italic;
`

const Component: FC<INewsItem> = ({ title, author, description, url, link }) => {
  return (
    <NewsItem href={url || link} target="_blank">
      <NewsItemHeader>{title}</NewsItemHeader>
      <NewsItemAuthor>{author}</NewsItemAuthor>
      <NewsItemDescription>{description}</NewsItemDescription>
    </NewsItem>
  );
};

Component.displayName = "NewsItem";

export default Component;
