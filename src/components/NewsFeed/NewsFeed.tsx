import React, { FC, useEffect, useState } from "react";
import * as Ably from "ably";
import NewsItem from "./NewsItem";
import { getLatestNews } from "./utils";

const Component: FC<{}> = () => {
  const [feed, setFeed] = useState([]);
  const [isFetchingData, setFetchingState] = useState(true);

  useEffect(() => {
    const ably = new Ably.Realtime(`${process.env.REACT_APP_API_KEY}`);
    const channelNews = ably.channels.get(
      "[product:ably-bbc/news]category:home"
    );
    channelNews.subscribe((message: any) => {      
      setFeed([message.data, ...feed] as any);
    });

    getLatestNews()
      .then(a => {
        setFeed(a.concat(feed));
        setFetchingState(false);
      })
  }, []);  

  return (
    <>
      <h1>News feed:</h1>
      {isFetchingData ? (
        <h1>Waiting for news ...</h1>
      ) : (
        feed.map((n, i) => <NewsItem key={i} {...n} />)
      )}
    </>
  );
};

Component.displayName = "NewsFeed";

export default Component;
