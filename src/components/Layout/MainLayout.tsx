import React, { FC, ReactNode } from "react";
import styled from "styled-components";

const MainLayout = styled.div`
  max-width: 700px;
  margin: 20px auto;
  padding: 0 30px;
`;

const Component: FC<{ children: ReactNode }> = ({ children }) => {
  return <MainLayout>{children}</MainLayout>;
};

Component.displayName = "MainLayout";

export default Component;
