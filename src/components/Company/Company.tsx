import React, { FC } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

import CompanyLogo from "./CompanyLogo";
import CompanyDescription from "./CompanyDescription";
import NewsFeed from "../NewsFeed/NewsFeed";
import dataMock from "../../dataMock";
import { ICompaniesListItem } from "../CompaniesList/types";
import { ICompanyUrlParams } from "./types";

const Component: FC<RouteComponentProps<ICompanyUrlParams>> = ({ match }) => {
  const companyData: ICompaniesListItem | undefined = dataMock.find(
    (c: ICompaniesListItem) => c.id === match.params.companyId
  );

  if (!companyData) {
    return (
      <>
        <h1>Something goes wrong!</h1>
        <Link to="/">Go to home page.</Link>
      </>
    );
  }

  return (
    <>
      <header>
        <Link to="/">{`< Back`}</Link>
      </header>
      <h1>{companyData.title} info page</h1>
      <CompanyLogo
        style={{width: '200px', height: '200px'}}
        src={`${companyData.logoUrl ? companyData.logoUrl + "?size=200" : ""}`}
      />
      <CompanyDescription>{companyData.description}</CompanyDescription>      
      <NewsFeed />
    </>
  );
};

Component.displayName = "Company";

export default withRouter(Component);
