import styled from "styled-components";

const CompanyLogo = styled.img`
  float: left;
  padding: 0 20px 20px 0;
`;

export default CompanyLogo;