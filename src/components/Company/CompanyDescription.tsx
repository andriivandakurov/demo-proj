import styled from "styled-components";

const CompanyDescription = styled.p({
  "::selection": {
    "background-color": "#f1c40f"
  }
});

export default CompanyDescription;