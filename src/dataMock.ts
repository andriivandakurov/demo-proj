export default [
  {
    id: "1",
    title: "Apple",
    logoUrl: "//logo.clearbit.com/apple.com",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in dui ut turpis consectetur varius id vitae dolor. Nullam sodales elementum urna et ultrices. Nam sit amet massa et metus cursus pellentesque in eu eros. Pellentesque ac ipsum in libero tristique volutpat. Morbi gravida mollis diam, nec ornare dolor. Sed vitae nibh nec neque pulvinar hendrerit. Donec nec nisl elit. Maecenas nisi est, vehicula et nisi ut, pharetra porttitor lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non risus nibh. Proin rhoncus scelerisque odio a imperdiet. Donec accumsan dolor purus, eget sagittis orci molestie eleifend. Ut iaculis consequat odio, sit amet aliquam est elementum eget. Nulla mauris quam, gravida in hendrerit ut, volutpat ut massa. Ut eu ligula non nibh interdum dictum. Ut convallis quis lectus ac lacinia. Mauris sit amet est cursus quam vehicula pretium. Aenean urna nisl, sagittis at tortor ac, gravida gravida ipsum. Quisque in lobortis sem. Vivamus molestie euismod faucibus. Fusce in enim feugiat, ultricies ante quis, convallis risus."
  },
  {
    id: "2",
    title: "Nike",
    logoUrl: "//logo.clearbit.com/nike.com",
    description:
      "Vivamus vitae eleifend metus. In sem odio, accumsan non arcu et, pretium laoreet mi. Ut porttitor, diam lacinia semper luctus, augue lorem sagittis arcu, eu luctus erat nisl sed felis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus sed lorem ac dolor fermentum sodales id ac sapien. Mauris eu tortor scelerisque, interdum ex quis, ullamcorper purus. Aliquam erat volutpat. Suspendisse vehicula mi sit amet sem imperdiet placerat. Ut iaculis, augue vitae efficitur ornare, sapien nunc elementum quam, eu ultricies diam tortor vitae ipsum. Vestibulum sit amet lacus placerat, aliquet ipsum a, pharetra eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non risus nibh. Proin rhoncus scelerisque odio a imperdiet. Donec accumsan dolor purus, eget sagittis orci molestie eleifend. Ut iaculis consequat odio, sit amet aliquam est elementum eget. Nulla mauris quam, gravida in hendrerit ut, volutpat ut massa. Ut eu ligula non nibh interdum dictum. Ut convallis quis lectus ac lacinia. Mauris sit amet est cursus quam vehicula pretium. Aenean urna nisl, sagittis at tortor ac, gravida gravida ipsum. Quisque in lobortis sem. Vivamus molestie euismod faucibus. Fusce in enim feugiat, ultricies ante quis, convallis risus."
  },
  {
    id: "3",
    title: "aliexpress",
    logoUrl: "//logo.clearbit.com/aliexpress.com",
    description:
      "Curabitur semper consectetur tincidunt. Sed a nisi pretium, semper massa dictum, varius lectus. Morbi tempor, lectus in feugiat porttitor, odio est laoreet eros, in suscipit eros odio varius eros. Aliquam vestibulum porta neque pretium tincidunt. Vestibulum auctor porttitor dictum. Nulla euismod tortor ut facilisis fringilla. Mauris eleifend cursus arcu vitae finibus. Vivamus at pharetra nibh, non feugiat quam. Nam maximus eleifend odio, quis rhoncus est mattis in. Nam ut magna in ante pharetra tristique. Nam efficitur leo vitae elit convallis faucibus. Quisque condimentum venenatis ex vitae facilisis. Praesent mattis sapien libero, id dapibus tellus rhoncus eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non risus nibh. Proin rhoncus scelerisque odio a imperdiet. Donec accumsan dolor purus, eget sagittis orci molestie eleifend. Ut iaculis consequat odio, sit amet aliquam est elementum eget. Nulla mauris quam, gravida in hendrerit ut, volutpat ut massa. Ut eu ligula non nibh interdum dictum. Ut convallis quis lectus ac lacinia. Mauris sit amet est cursus quam vehicula pretium. Aenean urna nisl, sagittis at tortor ac, gravida gravida ipsum. Quisque in lobortis sem. Vivamus molestie euismod faucibus. Fusce in enim feugiat, ultricies ante quis, convallis risus."
  },
  {
    id: "4",
    title: "pepsi",
    logoUrl: "//logo.clearbit.com/pepsi.com",
    description:
      "Curabitur semper consectetur tincidunt. Sed a nisi pretium, semper massa dictum, varius lectus. Morbi tempor, lectus in feugiat porttitor, odio est laoreet eros, in suscipit eros odio varius eros. Aliquam vestibulum porta neque pretium tincidunt. Vestibulum auctor porttitor dictum. Nulla euismod tortor ut facilisis fringilla. Mauris eleifend cursus arcu vitae finibus. Vivamus at pharetra nibh, non feugiat quam. Nam maximus eleifend odio, quis rhoncus est mattis in. Nam ut magna in ante pharetra tristique. Nam efficitur leo vitae elit convallis faucibus. Quisque condimentum venenatis ex vitae facilisis. Praesent mattis sapien libero, id dapibus tellus rhoncus eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non risus nibh. Proin rhoncus scelerisque odio a imperdiet. Donec accumsan dolor purus, eget sagittis orci molestie eleifend. Ut iaculis consequat odio, sit amet aliquam est elementum eget. Nulla mauris quam, gravida in hendrerit ut, volutpat ut massa. Ut eu ligula non nibh interdum dictum. Ut convallis quis lectus ac lacinia. Mauris sit amet est cursus quam vehicula pretium. Aenean urna nisl, sagittis at tortor ac, gravida gravida ipsum. Quisque in lobortis sem. Vivamus molestie euismod faucibus. Fusce in enim feugiat, ultricies ante quis, convallis risus."
  }
];
