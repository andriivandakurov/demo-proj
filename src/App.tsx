import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import CompaniesList from "./components/CompaniesList/CompaniesList";
import Company from "./components/Company/Company";
import MainLayout from "./components/Layout/MainLayout";
import dataMock from "./dataMock";

function App() {
  return (
    <MainLayout>
      <Router>
        <Switch>
          <Route path="/company/:companyId">
            <Company />
          </Route>
          <Route path="/">
            <CompaniesList companies={dataMock} />
          </Route>
        </Switch>
      </Router>
    </MainLayout>
  );
}

export default App;
